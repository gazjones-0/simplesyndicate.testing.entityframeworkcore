# README #

SimpleSyndicate.Testing.EntityFrameworkCore NuGet package.

Get the package from https://www.nuget.org/packages/SimpleSyndicate.Testing.EntityFrameworkCore

### What is this repository for? ###

* Common unit testing functionality for EntityFrameworkCore applications.

### How do I get set up? ###

* See the documentation at http://gazooka_g72.bitbucket.org/SimpleSyndicate

### Reporting issues ###

* Use the tracker at https://bitbucket.org/gazooka_g72/simplesyndicate.testing.entityframeworkcore/issues?status=new&status=open
