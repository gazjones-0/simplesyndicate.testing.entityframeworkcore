﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

using System;
using SimpleSyndicate.EntityFrameworkCore.Models;
using SimpleSyndicate.Repositories;

namespace SimpleSyndicate.Testing.TestData
{
    /// <summary>
    /// Contains a set of repositories that have been populated with a suite of test data, suitable for use in unit testing.
    /// </summary>
    /// <remarks>
    /// This class is designed to act as a base class that is then extended to hold other, application-specific, repositories.
    /// </remarks>
    public abstract class RepositoriesContainer : IDisposable
    {
        /// <summary>
        /// Gets or sets where Dispose has been called or not.
        /// </summary>
        private bool _disposed = false;

        /// <summary>
        /// Gets or sets the <see cref="VersionHistoryItem"/> repository.
        /// </summary>
        /// <value>The <see cref="VersionHistoryItem"/> repository.</value>
        public IRepository<VersionHistoryItem> VersionHistoryItemRepository { get; set; }

        /// <overloads>
        /// <summary>
        /// Releases all resources that are used by the current instance of the <see cref="RepositoriesContainer"/> class.
        /// </summary>
        /// </overloads>
        /// <summary>
        /// Releases all resources that are used by the current instance of the <see cref="RepositoriesContainer"/> class;
        /// <see cref="VersionHistoryItemRepository"/> will be released.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged resources and optionally releases managed resources; if <paramref name="disposing"/> is <c>true</c>
        /// <see cref="VersionHistoryItemRepository"/> will be released.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
            {
                return;
            }

            if (disposing)
            {
                if (VersionHistoryItemRepository != null)
                {
                    VersionHistoryItemRepository.Dispose();
                    VersionHistoryItemRepository = null;
                }
            }

            _disposed = true;
        }
    }
}
