﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

using SimpleSyndicate.EntityFrameworkCore.Models;
using SimpleSyndicate.Repositories;
using SimpleSyndicate.Testing.Repositories;

namespace SimpleSyndicate.Testing.TestData
{
    /// <summary>
    /// Factory class for creating repositories for <see cref="RepositoriesContainer"/>.
    /// </summary>
    public static class RepositoriesContainerFactory
    {
        /// <summary>
        /// Creates a <see cref="VersionHistoryItem"/> <see cref="InMemoryRepository{TEntity}"/>, populated with test data.
        /// </summary>
        /// <returns>A <see cref="VersionHistoryItem"/> repository.</returns>
        public static IRepository<VersionHistoryItem> CreateVersionHistoryItemRepository()
        {
            IRepository<VersionHistoryItem> repository = null;
            IRepository<VersionHistoryItem> tempRepository = null;
            try
            {
                tempRepository = new InMemoryRepository<VersionHistoryItem>();
                tempRepository.Add(new VersionHistoryItem() { MajorVersion = 1, MinorVersion = 0, PointVersion = 0 });
                tempRepository.Add(new VersionHistoryItem() { MajorVersion = 1, MinorVersion = 0, PointVersion = 1 });
                tempRepository.Add(new VersionHistoryItem() { MajorVersion = 1, MinorVersion = 0, PointVersion = 2 });
                tempRepository.Add(new VersionHistoryItem() { MajorVersion = 2, MinorVersion = 0, PointVersion = 0 });
                tempRepository.Add(new VersionHistoryItem() { MajorVersion = 2, MinorVersion = 1, PointVersion = 0 });
                tempRepository.Add(new VersionHistoryItem() { MajorVersion = 2, MinorVersion = 1, PointVersion = 1 });
                repository = tempRepository;
                tempRepository = null;
            }
            finally
            {
                if (tempRepository != null)
                {
                    tempRepository.Dispose();
                }
            }

            return repository;
        }
    }
}
